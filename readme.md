# sendgrid cancel scheduled sends の使い方メモ

sendgrid には配信を取りやめる機能があります。それには batch_id を使うが、それの使い方を確認するメモ。

# ここで書いている言葉の事

メールの *送信* と *配信* の意味に差をつけてます。

- メールを、こちらからSendGridに渡す事を *送信* と書いています。
- メールを、SendGridから宛先まで渡す事を *配信* と書いています。

ただし、引用文などは変えずにそのまま記載していますので、注意してください。

- `batch_id` は sendgird が発行した、メールに紐付けて対象メールの配信をキャンセルを行う為の id です
- `send_at` はプログラム側から sendgrid に配信時刻を設定する時刻の事？

## 参考

- https://sendgrid.api-docs.io/v3.0/mail-send/v3-mail-send
- https://sendgrid.kke.co.jp/docs/API_Reference/Web_API_v3/cancel_schedule_send.html

# console コマンドの事

`.env.example` を `.env` にファイルコピーをして、内容の値に適切に設定をしてください。

`batch_id` を付与して送信したらどうなるかを見るために作ったテストプログラムです。それほど機能はないが、後々増えるかも。意外と `batch_id` の操作は、単体プログラムが無いから、必要になる気がする。

# `cancel scheduled sends` の使い方

1. `batch_id` を作成する
2. send_at で一時間後の時間を配送日時に指定して、取得した `batch_id` を指定して、メールを送信をする
3. それを cancel scheduled sends で停止させる。
4. 停止したメールを削除する

## 注意点1

[Delete a cancellation or pause state of a scheduled send](https://sendgrid.kke.co.jp/docs/API_Reference/Web_API_v3/cancel_schedule_send.html#Delete-a-cancellation-or-pause-state-of-a-scheduled-send-DELETE) には下記のように書かれています。

> キャンセルまたは一時停止状態のスケジュール配信を削除します。このバッチIDが指定されている送信キュー内のメッセージはアクティブ状態に戻り、 send_at で指定した時刻になると送信が試みられます。送信予定時刻を過ぎた一時停止中のメッセージはすぐに送信が試みられます。送信予定時刻を過ぎたキャンセル済のメッセージは破棄されます。

要は、ちゃんと `batch_id` をキャンセル状態にしておかなければ、一時停止状態のまま削除メソッドを実行するとメールは配信する、との事。

## 注意点2

https://sendgrid.com/docs/API_Reference/Web_API_v3/API_Keys/api_key_permissions_list.html#Mail

`batch_id` を作成閲覧する権限が必要になる。 <CANCEL SCHEDULED SENDS> のフルアクセス権限が必要になるので、APIKeyの設定を確認するように。

## 注意点3

[Batch IDは作成してから10日間は有効](https://sendgrid.kke.co.jp/docs/API_Reference/Web_API_v3/cancel_schedule_send.html#Generate-Batch-ID-POST)です。

意図的に `batch_id` を削除するか再送信をしない限り残り続ける。増える一方なので、管理しきれなくなる。

### 対処法

1. `batch_id` は複数のメールに割り当てることが出来るので、大量配信でも一つの `batch_id` として管理することが出来る。
2. `cancel` した `batch_id` は、すぐに削除をする。配信時刻まで残り続けるが、時間経過したら削除され、`batch_id` もリストから消える。

    1. その間、メールも Activity Feed の画面では `Processed` 状態で残る事に注意。

## 注意点4

送信する時の `send_at` は `UTC` の時刻になるように。`unix timestamp` だから `UTC` にはなるはず。

そして、sendgrid のブラウザで見る画面に表示される送信日時などは、これはアカウント設定でしているため、JST の時刻になっている。

# memo

1. cancel schedule はメールに関連している。従って、sendgrid の Activity Feed からは `batch_id` を見ることはできない。

    というか、どこにも cancel schedule の画面がない。したがって、`cancel schedule`の状態管理などをするには、こちら側でほぼ実装しなければならない。

1. キャンセル/一時停止の一覧の取得はできる。

    取得できる内容は下記に記すが、`batch_id` と `status` のみであり、それ以上の情報はない。
    そのため、 `batch_id` と送信したメールの関連性は *確実* に保つ必要がある。

1. キャンセルしたメールは、一時停止状態に変更することはできない気配

    - 一時停止にしたメールはキャンセル状態に出来る。
    - 一時停止状態のメールを `DELETE /user/scheduled_sends/{batch_id}` で削除をしたら配信されてしまう事に注意
    
        - 削除するには 

# API

順番はプログラムを書く上で把握しておく必要がある順番。

## POST /mail/batch

see: https://sendgrid.api-docs.io/v3.0/cancel-scheduled-sends/create-a-batch-id

メールの配信をキャンセルする為の ID を取得する。メールを送信する時に、取得した `batch_id` を割り当てて流す。

- 発行したIDは10日間有効

    - なんでそんなに長いんだ？

- 複数のメールに一つの `batch_id` を割り当てることは可能。

    - 推測: `send_at` で指定している配信する時刻の10分前までに送信したメールに付与することが可能。
    - よくわからない点: 一番始めに送られてきたメールの `send_at` が制限時間になるのか、送り続けられてきたメールで最寄りの `send_at` が制限時間になるかは不明
    - 10分なのは [Cancel Scheduled Sends](https://sendgrid.com/docs/API_Reference/Web_API_v3/cancel_schedule_send.html#Cancel-or-pause-a-scheduled-send-POST)に書いている確実に配信が止められる制限時間
    - そもそも `send_at` がない場合はどうなるか不明
    - `personalization` にある `send_at` も見るよな？
    - 推測: 例えば1時間後のメール(A)、30分後のメール(B)、の二種類ある時に、 B 送信後に A の配信停止は出来る？

### Response::

```json
{
  "batch_id": "<json>"
}
```

## GET /user/scheduled_sends/{batch_id}

see : https://sendgrid.api-docs.io/v3.0/cancel-scheduled-sends/retrieve-scheduled-send

一時停止もしくはキャンセルをした `batch_id` の状態を返す。

- 一時停止もしくはキャンセルしたIDしか掲載されていない。送信/配信/削除をしたメールないしは期限切れの `batch_id` は掲載されない。

### Response::

```json
[
  {
    "batch_id": "<batch id>",
    "status": "cancel"
  }
]
```

## PATCH /user/scheduled_sends/{batch_id}

see: https://sendgrid.api-docs.io/v3.0/cancel-scheduled-sends/update-user-scheduled-send-information

指定した `batch_id` の状態を変更させる。

変更には時間がかかるっぽい。実行してから数分ぐらい待つ必要があるので、実行は数分に一回、[GET /user/scheduled_sends](https://sendgrid.api-docs.io/v3.0/cancel-scheduled-sends/retrieve-all-scheduled-sends) で指定IDが記載された時のみ実行できる感じにする必要がある。

### Response:: `cancel` -> `pause`

- 204

body は空

### Response:: `pause` -> `pause`

- 404

body は空

### Response:: `pause` -> `cancel`

- 204

body は空

## DELETE /user/scheduled_sends/{batch_id}

see: https://sendgrid.api-docs.io/v3.0/cancel-scheduled-sends/cancel-or-pause-a-scheduled-send

指定した `batch_id` を削除、もしくは再送を試みる。

- `pause` の状態の時にこのメソッドを実行すると、再送を実行する。`send_at` で指定した時間を過ぎている場合は直ぐに送信され、まだその時間の10分前であるなら、その時間に送信が試みられる。
- 実行完了までに時間かかるっぽい。実行は数分に一回、[GET /user/scheduled_sends](https://sendgrid.api-docs.io/v3.0/cancel-scheduled-sends/retrieve-all-scheduled-sends) で指定IDが記載された時のみ実行できる感じにする必要がある。

## GET /user/scheduled_sends

see: https://sendgrid.api-docs.io/v3.0/cancel-scheduled-sends/retrieve-all-scheduled-sends

キャンセルもしくは配信停止にしている `batch_id` の一覧を取得する。


### Response::

```json
[
  {
    "batch_id": "<batch id 1>",
    "status": "cancel"
  },
  {
    "batch_id": "<batch id 2>",
    "status": "pause"
  },
  {
    "batch_id": "<batch id 3>",
    "status": "pause"
  },
  {
    "batch_id": "<batch id 4>",
    "status": "cancel"
  }
]
```


## GET /mail/batch/{batch_id}

see: https://sendgrid.api-docs.io/v3.0/cancel-scheduled-sends/validate-batch-id

指定した `batch_id` が有効か確認をする。有効であれば `200` を返すっぽいけど、`DELETE /user/scheduled_sends/{batch_id}`などを実行した直後でも `200` を返す事に注意。

### Response::

```json
{
  "batch_id": "<batch id>"
}
``` 

## SendGrid の挙動的な考察

個人の感想。

1. sendgrid はメールをそれぞれ状態に合わせて DB で移動させてるような感じで扱っているように思う。

    1. 思想的には Postfix にかなり近い。Postfix のシステム設計を巨大なシステムに当てはめた感じ。
    
        1. 補足: Postfix の思想として、状態に応じたプログラムはそれぞれ独立している、という考え方である。メールには様々な状態がある。その状態に応じたプログラムが責任を持ち、postfix の設定した次のプログラムへ渡す。そうすることで、途中に SpamFilter や、アンチウイルスフィルターといったフィルタープログラム、ユーザ認証まわりを MySQL にあるユーザデータベースを見たり、PostgreSQL にあるユーザデータベースを見たり、送信ログをどこかに保存する、等と言ったプログラムを気楽に入れられるようになる。
        1. 何故そんなプログラムができたとか、歴史的背景は割愛。ここに書くには重すぎるのと、関係がないため。
    
    1. そのために、キャンセルするまでの時間が、配信時間の10分前というのは配信プログラムの受け渡しが10分前とかに開始されると推測できる。

1. `batch_id` などは後付けで追加された機能であり、`Processed` から `Delivered` へ移る時に通るフィルターの一つ

    1. だから `batch_id` の状態が `cancelled`,`porse` のどちらかのときは、メールは `Processed` に戻っている気がする。
    1.  ~~ただし `cancelled` が `porse` にはならないので、メールはそのまま `Delete` になる気がする。~~ 気の所為でした。